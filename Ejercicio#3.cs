//3.Un programa que almacene en un array el número de días que tiene cada mes (supondremos que es un año no bisiesto), pida al usuario que le indique un mes (1=enero, 12=diciembre) y muestre en pantalla el número de días que tiene ese mes.

using System;

namespace Ejercicios
{
    class Program
    {
        static void Main(string[] args)
        {   
            String[] meses = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre" ,"Octubre", "Noviembre", "Diciembre"};
            var mesdigitado= 0;

            while (mesdigitado <= 0 || mesdigitado > 12)
            {
                Console.WriteLine("Ingrese un número del 1 al 12.");
               mesdigitado =  int.Parse(Console.ReadLine());
            }


            var cantidadDias = System.DateTime.DaysInMonth(2021, mesdigitado);

            Console.WriteLine("Los dias en el mes de " + meses[mesdigitado - 1] + " hay " + cantidadDias + " días");


        }
    }
}
