//8.Crear un programa que defina un array de 5 elementos de tipo float que representen las alturas de 5 personas.
//Obtener el promedio de las mismas. Contar cuántas personas son más altas que el promedio y cuántas más bajas.

using System;
using System.Linq;

namespace ejercicio8
{
    class Program
    {
        static void Main(string[] args)
        {
            float[] altura = new float[5];
            float promedio =0;

            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Digite la altura de la persona"+" " +(i+1));
                altura [i]= float.Parse(Console.ReadLine());
                promedio = promedio + altura[i];
            }
                Console.WriteLine("El promedio de la altura es:" + promedio / 5);
                Console.WriteLine("La persona: " + altura.Max());
                Console.WriteLine("La persona que tiene el promedio mas bajo" + altura.Min());
        }
    }
}
