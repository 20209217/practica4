//4.Un programa que pida al usuario 10 números y luego calcule y muestre cuál es el mayor de todos ellos.

using System;
using System.Linq;

namespace ejercicio4
{
    class Program
    {
        static void Main(string[] args)
        {
            int [] numeros = new int[10];
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Digite numero: "+ (i+1));
                numeros [i] = int.Parse(Console.ReadLine());
            }
            
            Console.WriteLine("El numero mayor es: " + numeros.Max());
        }
    }
}
