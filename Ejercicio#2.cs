//2.Un programa que pida al usuario 5 números reales (pista: necesitarás un array de "float") y luego los muestre en el orden contrario al que se introdujeron.

using System;
using System.Linq;

namespace ejercicio3
{
    class Program
    {
        static void Main(string[] args)
        {
            float[] numero = new float[5];
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Digite el número: ");
                numero[i] = float.Parse(Console.ReadLine());
            }

            float[] arrayReverso = numero.Reverse().ToArray();

                Console.WriteLine("Numeros en orden reverso");

            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(arrayReverso[i]);
            }


        }
    }
}
