//9.Crear una clase que permita ingresar valores enteros por teclado y nos muestre la tabla de multiplicar de dicho valor. Finalizar el programa al ingresar el -1.
using System;

namespace ejercicio9
{
    class Program
    {
        static void Main(string[] args)
        {  
            int n;
            string linea;
            while (n!=-1)
            {
   
            Console.Write("Ingrese multiplicador: ");
            linea = Console.ReadLine();
            n= int.Parse(linea);  
            }
            
            for (int i = 0; i <= 15; i++)
            {
                Console.Write(i +" x "+n+" = "+i*n+"\n");
                
            }
        }
    }
}
