//5.Un programa que prepare espacio para un máximo de 100 nombres. El usuario deberá ir introduciendo un nombre cada vez, hasta que se pulse Intro sin teclear nada, momento en el que dejarán de pedirse más nombres y se mostrará en pantalla la lista de los nombres que se han introducido.

using System;

namespace ejercicio5
{
    class Program
    {
        static void Main(string[] args)
        {
            string [] nombres = new string[100];
            for (int i = 0; i < 100; i++)
            {
               Console.WriteLine("Digite los nombres: " + (i+1));
               var nombreDigitado = Console.ReadLine(); 

               if(nombreDigitado == null || nombreDigitado == "")
                break;

                nombres[i] = nombreDigitado;
            }

            Console.WriteLine("Nombres Digitados: ");

            for (int i = 0; i < nombres.Length; i++)
            {
                if(nombres[i] == null)
                    continue;

                Console.WriteLine(nombres[i]);
            }
            
      
        }
    }
}
